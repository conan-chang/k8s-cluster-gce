terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.6.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
  }
}

provider "google" {
  project     = var.project
  region      = var.region
  zone        = var.zone[0]
  credentials = var.key_file_path
}

# provider google-beta {
#   project     = var.project
#   region      = var.region
#   zone        = var.zone
#   credentials = "key.json"

# }


provider "random" {
  # Configuration options
}
# data "google_client_config" "current" {}

