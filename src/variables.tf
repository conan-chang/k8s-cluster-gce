variable "key_file_path" {
  description = "Your credenial key file"
  type        = string
  default     = "key.json"
}

variable "main_name" {
  description = "The name used for create resource"
  type = string
  default = "k8s"
}

variable "project" {
  description = "GCP project id"
  type        = string
  default     = "project-id"
}

variable "region" {
  description = "Region in VPC network, all resouce allocate to this region"
  type        = string
  default     = "asia-east1"
}

variable "zone" {
  description = <<-EOF
  The default zome of all resource, selected by order. 
  For example, two zone node pool will allocate to zone[0] and zone[1]"
  EOF
  type        = list(string)
  default = [
    "asia-east1-b",
    "asia-east1-a",
    "asia-east1-c"
  ]
}

variable "gcp_api_services" {
  description = "The GCP api we need"
  type        = list(string)
  default = [
    "compute.googleapis.com",
  ]
}

variable "machines_name" {
  description = "The main(middle) name of GCE"
  type        = map(string)
  default = {
    "machine1" : "master1",
    "machine2" : "master2",
    "machine3" : "master3",
    "machine4" : "worker1",
    "machine5" : "worker2",
  }
}

variable "gce_service_account" {
  description = "The service account used by GCE"
  type = map(string)
  default = {
    "account_id": "gce-default-sa",
    "display_name": "GCE default service account"
  }
}