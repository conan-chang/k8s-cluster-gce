# Enable GCP API service
resource "google_project_service" "service" {
  count   = length(var.gcp_api_services)
  project = var.project
  service = element(var.gcp_api_services, count.index)

  # Keep the service API enabled for the project
  disable_on_destroy = false
}