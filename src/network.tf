# VPC
resource "google_compute_network" "gce" {
  name                    = format("%s-network-%s", var.main_name, random_id.random_id.hex)
  project                 = var.project
  auto_create_subnetworks = true

  depends_on = [
    google_project_service.service
  ]
}

# Cloud router for NAT
resource "google_compute_router" "nat-router" {
  name    = format("%s-cloud-router-%s", var.main_name, random_id.random_id.hex)
  project = var.project
  region  = var.region
  network = google_compute_network.gce.self_link

  bgp {
    asn = 64514
  }
}

# Cloud router for Cloud NAT
resource "google_compute_router_nat" "nat" {
  name    = format("%s-cloud-nat-%s", var.main_name, random_id.random_id.hex)
  project = var.project
  router  = google_compute_router.nat-router.name
  region  = var.region

  nat_ip_allocate_option = "AUTO_ONLY"

  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

}
