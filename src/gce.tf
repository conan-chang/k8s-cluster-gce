resource "google_service_account" "gce" {
  account_id   = var.gce_service_account.account_id
  display_name = var.gce_service_account.display_name
  project      = var.project
}

resource "google_compute_instance" "k8s_instance" {
  for_each = var.machines_name
  name         = each.value
  machine_type = "e2-medium"
  zone         = var.zone[0]

  boot_disk {
    # gcloud compute images list
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"  # or centos-cloud/centos-7
    }
  }

  network_interface {
    network = google_compute_network.gce.self_link

    access_config {
      // Ephemeral public IP
    }
  }

  # metadata = {
  #   foo = "bar"
  # }

  metadata_startup_script = "echo hi > /test.txt"

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = google_service_account.gce.email
    scopes = ["cloud-platform"]
  }
}